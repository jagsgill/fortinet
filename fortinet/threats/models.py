from __future__ import unicode_literals
from django.db import models


class Threat(models.Model):
    RATING_CLEAN = 'clean'
    RATING_LOW = 'low-risk'
    RATING_MED = 'medium-risk'
    RATING_HIGH = 'high-risk'
    RATING_MAL = 'malicious'
    RATING_UNKNOWN = 'unknown'
    RATING_CHOICES = [
        (RATING_CLEAN, "Clean"),
        (RATING_LOW, "Low-Risk"),
        (RATING_MED, "Medium-Risk"),
        (RATING_HIGH, "High-Risk"),
        (RATING_MAL, "Malicious"),
        (RATING_UNKNOWN, "Unknown"),
    ]

    date = models.DateTimeField()
    filename = models.CharField(max_length=1000)
    action = models.CharField(max_length=1000)
    submit_type = models.CharField(max_length=1000)
    rating = models.CharField(max_length=1000, choices=RATING_CHOICES, default=RATING_UNKNOWN) 

