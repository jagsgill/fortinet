import os
import json
import time
import shutil
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from threats.models import Threat

class Command(BaseCommand):
    help = "Start processing threat meta-files"
    FILES_BASEDIR = os.path.join(settings.BASE_DIR, "threatfiles") 
    FILES_NEW = os.path.join(FILES_BASEDIR, "new")
    FILES_OLD = os.path.join(FILES_BASEDIR, "old")

    def handle(self, *args, **options):
        a = True
        while a:
            for fname in os.listdir(self.FILES_NEW):
                self.process_threatfile(fname, self.FILES_NEW, self.FILES_OLD) 
            time.sleep(5) 

    def process_threatfile(self, fname, dir_new, dir_old):
        CURR_PATH = os.path.join(dir_new, fname)  #original location
        NEW_PATH = os.path.join(dir_old, fname)  #where file is moved

        with open(CURR_PATH, 'r') as threat_file:
            threat_list = json.load(threat_file)
        for threat in threat_list:
            t,created = Threat.objects.update_or_create(
                date=datetime.strptime(threat['date'], '%b %d, %Y %H:%M:%S'),
                filename=threat['filename'],
                action=threat['action'],
                submit_type=threat['submit-type'],
                rating=threat['rating']
                )
        self.stdout.write(self.style.SUCCESS('Processed %s' % fname))
        i = 1
        while os.path.exists(NEW_PATH):
            NEW_PATH = os.path.join(dir_old, fname + "_" + str(i)) 
            i += 1
        shutil.move(CURR_PATH, NEW_PATH)
        self.stdout.write('Moved file %s to %s ' % (CURR_PATH, NEW_PATH))
        
