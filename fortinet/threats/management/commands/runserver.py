import os
import subprocess
from threading import Thread
from django.contrib.staticfiles.management.commands.runserver import Command as BaseCommand
from django.conf import settings

# Adapted from http://stackoverflow.com/questions/29351994/how-to-automatically-run-tests-when-running-django-runserver
# and http://stackoverflow.com/questions/34164799/custom-runserver-command-gets-404-on-static-files


class Command(BaseCommand):
    def inner_run(self, *args, **options):
        #if settings.DEBUG and not settings.TESTING:
        self.run_threat_processor()
        super(Command, self).inner_run(*args, **options)


    def call_then_log(self):
        try:
            output = subprocess.Popen('python manage.py processthreatfiles', shell=True, stderr=subprocess.STDOUT)
        except Error as ex:
            print(ex.output)
            return
        print(output)


    def run_threat_processor(self):
        print('Running threat meta-file processor...')
        thread = Thread(target=self.call_then_log, name='runserver-background-threatmetafile-processor')
        thread.daemon = True
        thread.start()


