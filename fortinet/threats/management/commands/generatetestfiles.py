import os
import json
import time
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings

class Command(BaseCommand):
    help = "Generating test files ..."
    FILES_BASEDIR = os.path.join(settings.BASE_DIR, "threatfiles") 
    FILES_NEW = os.path.join(FILES_BASEDIR, "new")

    def handle(self, *args, **options):
        result = []
        risk_level = ["clean", "low-risk", "medium-risk", "high-risk", "malicious", "unknown"]
        period = options['period']
        seed = options['seed']
        for risk in risk_level:
            threat = {}
            date = datetime.now() - timedelta(days=int(period))
            fdate = date.strftime('%b %d, %Y %H:%M:%S')
            threat["date"] = fdate
            threat["filename"] = "virus.exe " + str(seed)
            threat["action"] = "test"
            threat["submit-type"] = "test/root"
            threat["rating"] = risk
            result.append(threat)
        as_json = json.dumps(result, cls=DjangoJSONEncoder)
        loc = os.path.join(self.FILES_NEW, "6.txt")
        with open(loc, "w+") as f:
            f.write(as_json)
        self.stdout.write(self.style.SUCCESS("Generated test file at " + loc)) 
        return

    def add_arguments(self, parser):
        parser.add_argument('seed', type=int)
        parser.add_argument('period', type=int) 
