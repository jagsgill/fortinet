from django.shortcuts import render 
from django.core import serializers
from django.http import JsonResponse
from threats.models import Threat
from datetime import datetime, timedelta


# Create your views here.

def index(request):
    threats = Threat.objects.all()
    return render(request, 'threats/index.html', { 'threatlist' : threats })

def period(request, time_period):
    delta = {
            '24h' : timedelta(days=1),
            '7d' : timedelta(days=7),
            '4w' : timedelta(days=28),
            }[time_period]
    startdate = datetime.now() - delta
    threats = Threat.objects.filter(date__gte=startdate).values('date', 'filename', 'action', 'submit_type', 'rating')
    return JsonResponse(list(threats), safe=False)
