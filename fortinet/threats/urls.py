from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<time_period>[A-Za-z0-9]+)/$', views.period, name='period'),
]

